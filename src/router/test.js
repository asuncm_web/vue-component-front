export const TestRoutes = [
  {
    name: '122',
    path: '/error',
    component: () => import('@/views/error'),
    alias: '/404'
  },
  {
    name: '500',
    path: '/error',
    component: () => import('@/views/error'),
    alias: '/500'
  },
  {
    name: '444',
    path: '/',
    component: () => import('@/views/error')
  }
]
