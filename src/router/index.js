import Vue from 'vue'
import VueRouter from 'vue-router'

// 引入基础模版路由
import { BasicRoutes } from './basic'
import { TestRoutes } from '@/router/test'

Vue.use(VueRouter) // 实例化路由

const router = new VueRouter({
  mode: 'history',
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) { // 浏览器前进后退按钮监听
      return savedPosition
    } else if (to.hash) { // 模版中锚点监听
      return {
        selector: to.hash
      }
    } else { // 监听history模式下的pushState状态
      return { x: 0, y: 0 }
    }
  },
  routes: BasicRoutes
})
router.options.routes = [...BasicRoutes, ...TestRoutes]
router.addRoutes(TestRoutes)
console.log(router, ',,,,')
export default router
