export const BasicRoutes = [
  {
    name: '404',
    path: '/error',
    component: () => import('@/views/error'),
    alias: '/404'
  },
  {
    name: '500',
    path: '/error',
    component: () => import('@/views/error'),
    alias: '/500'
  },
  {
    name: 'home',
    path: '/',
    component: () => import('@/views/error')
  }
]
