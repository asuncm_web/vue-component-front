import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'

import App from './App'
import store from './store'
import router from './router'

Vue.use(ElementUI, { zhLocale })
Vue.config.productionTip = false

import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
Vue.use(preview)

// 全局confirm确认
Vue.prototype.$openConfirm = function(title, trueCallback, falseCallback) {
  Vue.prototype.$confirm(title, '提示', {
    confirmButtonText: '确定',
    cancelButtonText: '取消',
    type: 'warning'
  })
    .then(() => trueCallback && trueCallback())
    .catch(() => {
      this.$message.info('操作已取消')
      falseCallback && falseCallback()
    })
}

new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App)
})
