# 酉艺管理系统前端
> 这是一个极简的 vue admin 管理后台。它只包含了 Element UI & axios & iconfont & permission control & lint，这些搭建后台必要的东西。

## 构建方法

```bash
# 先拉取代码，然后进入代码文件夹，运行
npm install

# build for local development
npm run dev

# build for test environment
npm run build:stage

# build for production environment
npm run build:prod
```

